### ReactJS Monaco

Monaco Editor based on VS code 

## Examples

To build locally in root folder, run:

```bash
yarn
yarn start
```

Then open `http://localhost:8886` in a browser.

## Installation

```bash
yarn add react-monaco-editor
```

Based on [Root Repo](https://github.com/react-monaco-editor/react-monaco-editor)

